﻿using App.helpers;
using App.interfaces;
using App.models;
using App.services;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App.popup
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EditNamePopup : PopupPage
	{
        ListItemObject Item;
		public EditNamePopup (ListItemObject item)
		{
			InitializeComponent ();
            this.Item = item;

            NameEntry.Text = this.Item.NameObject;
            TitleLbl.TextColor = Color.FromHex(Session.ConnectedUser.Colore);
        }

        private async void OnConfirmEditNameCliked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(NameEntry.Text) && !string.IsNullOrWhiteSpace(NameEntry.Text))
            {
                ItemUpdateParam param = new ItemUpdateParam() { Id = Item.Id, Title = NameEntry.Text };

                var result = await ServiceClient.SendData("UpdateItem", param);

                if (result != null)
                {
                    bool response = JsonConvert.DeserializeObject<bool>(result);

                    if (response)
                    {
                        MessagingCenter.Send<object, object>(this, NotificationHelper.ONMODIFYLIST, param);

                        DependencyService.Get<IGetToast>().GetToast("Elemento modificato!", string.Empty, false);

                        await Navigation.PushModalAsync(new NavigationPage(new MainPage()) { BarBackgroundColor = Color.FromHex(Session.ConnectedUser.Colore), BarTextColor = Color.FromHex("#fff") });
                    }
                    else
                    {
                        DependencyService.Get<IGetToast>().GetToast("Modifica non riuscita, riprova più tardi", string.Empty, false);
                    }
                }
                else
                {
                    DependencyService.Get<IGetToast>().GetToast("Modifica non riuscita, riprova più tardi", string.Empty, false);
                }

                Session.PlacesList.Where(x => x.Id == Item.Id).FirstOrDefault().NameObject = NameEntry.Text;

                MessagingCenter.Send<object, object>(this, NotificationHelper.ONMODIFYLIST, this.Item);

                await Navigation.PopPopupAsync();
            }
            else
            {
                DependencyService.Get<IGetToast>().GetToast("Devi inserire un nome!", string.Empty, false);
            }
        }
    }
}