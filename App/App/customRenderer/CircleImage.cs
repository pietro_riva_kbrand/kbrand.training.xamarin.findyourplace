﻿using Xamarin.Forms;

namespace App.customRenderer
{
    public class CircleImage : Image
    {
        public Color OutlineColor { get; set; }
        public Color ShadowColor { get; set; }
        public ClipType ClipType { get; set; }
        public int StrokeWidth { get; set; }
        public bool HasShadow { get; set; }
        public float ShadowWidth { get; set; }
        public float RectRadius { get; set; }
    }

    public enum ClipType
    {
        Circle,
        Square
    }
}
