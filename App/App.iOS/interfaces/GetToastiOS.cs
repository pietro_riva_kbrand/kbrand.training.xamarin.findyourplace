﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using App.interfaces;
using App.iOS.interfaces;
using Foundation;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(GetToastiOS))]
namespace App.iOS.interfaces
{
    class GetToastiOS : IGetToast
    {
        public void GetToast(string message, string title, bool DelayLong)
        {
            UIAlertView alert = new UIAlertView()
            {
                Title = title,
                Message = message
            };
            alert.AddButton("OK");
            alert.Show();
        }
    }
}