﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace App.services
{
    public class ServiceClient
    {
        public static async Task<string> SendData(string method, Object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            string url = $"{Session.Base_URL}/{method}";

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(90);

                    StringContent objectContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.PostAsync(url, objectContent);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        public static async Task<string> SendDataContent(string method, Object obj, byte[] file)
        {
            var json = JsonConvert.SerializeObject(obj);
            string url = $"{Session.Base_URL}{method}";
            MultipartFormDataContent form = new MultipartFormDataContent();
            

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(120);
                    
                    form.Add(new StringContent(json), "item");
                    form.Add(new ByteArrayContent(file, 0 , file.Length), "photo");                    


                    HttpResponseMessage response = await httpClient.PostAsync(url, form);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = await response.Content.ReadAsStringAsync();
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        public static async Task<object> GetData(string method, Object obj, string param = "sParamsIn")
        {
            var json = JsonConvert.SerializeObject(obj);
            string url = $"{Session.Base_URL}/{method}?{param}={json}";

            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(90);

                    StringContent objectContent = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await httpClient.GetAsync(url);

                    if (response.IsSuccessStatusCode)
                    {
                        var result = JsonConvert.DeserializeObject<object>(await response.Content.ReadAsStringAsync());
                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return null;
            }
        }
    }
}
