﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.interfaces
{
    public interface IGetToast
    {
        void GetToast(string message, string title, bool delayLong);
    }
}
