﻿using App.helpers;
using App.interfaces;
using App.models;
using App.popup;
using App.services;
using Newtonsoft.Json;
using Plugin.FirebasePushNotification;
using Plugin.Geolocator.Abstractions;
using Plugin.Media;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            PopulateListView();

            MessagingCenter.Subscribe<object, object>(this, NotificationHelper.ONMODIFYLIST, (sender, args) =>
            {
                ListViewObject.ItemsSource = null;
                ListViewObject.ItemsSource = Session.PlacesList;
                PopulateListView();
            });

            CrossFirebasePushNotification.Current.OnNotificationReceived += Current_OnNotificationReceived;

            CrossFirebasePushNotification.Current.OnNotificationOpened += Current_OnNotificationOpened;
        }

        private void Current_OnNotificationOpened(object source, Plugin.FirebasePushNotification.Abstractions.FirebasePushNotificationResponseEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Data);
        }

        private void Current_OnNotificationReceived(object source, Plugin.FirebasePushNotification.Abstractions.FirebasePushNotificationDataEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Data);
        }

        private async void PopulateListView()
        {
            try
            {
                ListViewObject.IsVisible = false;
                IndicatorLayout.IsVisible = true;

                GetListParam param = new GetListParam() { UserId = Session.ConnectedUser.Id };

                var result = await ServiceClient.SendData("GetItemList", param);

                if (result != null)
                {
                    Session.PlacesList = JsonConvert.DeserializeObject<List<ListItemObject>>(result);

                    if (Session.PlacesList.Count == 0)
                    {
                        NoItemLayout.IsVisible = true;
                        ListViewObject.IsVisible = true;
                        IndicatorLayout.IsVisible = false;
                    }
                    else
                    {
                        ListViewObject.ItemsSource = Session.PlacesList;
                        ListViewObject.IsVisible = true;
                        IndicatorLayout.IsVisible = false;
                        NoItemLayout.IsVisible = false;
                    }
                }
                else
                {
                    NoItemLayout.IsVisible = true;
                    ListViewObject.IsVisible = true;
                    IndicatorLayout.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                NoItemLayout.IsVisible = true;
                ListViewObject.IsVisible = true;
                IndicatorLayout.IsVisible = false;
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        private async void OnListItemselected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (ListItemObject)e.SelectedItem;
            await Navigation.PushAsync(new NavigationPage(new DetailPage(item)) { BarBackgroundColor = Color.FromHex(Session.ConnectedUser.Colore), BarTextColor = Color.FromHex("#fff"), Title=item.NameObject });
        }

        private async void OnHeaderListClicked(object sender, TappedEventArgs e)
        {
            try
            {
                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported || !CrossMedia.Current.IsPickPhotoSupported)
                {
                    return;
                }
                else
                {
                    var statusCamera = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);

                    if (statusCamera != PermissionStatus.Granted)
                    {
                        if (Device.RuntimePlatform == Device.Android)
                        {
                            var resultCamera = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
                            if (resultCamera.Where(x => x.Key == Permission.Camera).FirstOrDefault().Value != PermissionStatus.Granted)
                            {
                                return;
                            }
                        }
                    }

                    var statusStorage = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);

                    if (statusStorage != PermissionStatus.Granted)
                    {
                        if (Device.RuntimePlatform == Device.Android)
                        {
                            var resultStorage = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                            if (resultStorage.Where(x => x.Key == Permission.Storage).FirstOrDefault().Value != PermissionStatus.Granted)
                            {
                                return;
                            }
                        }
                    }

                    var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions()
                    {
                        Directory = "FindMyPlace",
                        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                        Name = $"{Session.ConnectedUser.Nome}_{DateTime.Now.Ticks}.jpg",
                        DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Rear,
                        RotateImage = true,
                        CompressionQuality = 60
                    });

                    if (photo != null)
                    {
                        await Navigation.PushModalAsync(new AddNewItem(photo.GetStream()));
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        private async void OnEditPlaceName(object sender, TappedEventArgs e)
        {
            var item = (ListItemObject)e.Parameter;
            await Navigation.PushPopupAsync(new EditNamePopup(item));
        }

        private async void OnDeletePlace(object sender, TappedEventArgs e)
        {
            var item = (ListItemObject)e.Parameter;
            await Navigation.PushPopupAsync(new DeleteItemPopup(item));
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }
    }
}
