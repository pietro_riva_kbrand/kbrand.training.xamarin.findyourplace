﻿using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.models
{
    public class ListItemObject
    {
        public ListItemObject()
        {

        }

        public ListItemObject(int id, string nameObject, string photoObject, Position positionObject, string address, string latitude, string description, string longitude)
        {
            Id = id;
            NameObject = nameObject;
            PhotoObject = photoObject;
            Address = address;
            Latitude = latitude;
            Longitude = longitude;
            Description = description;
        }

        public int Id { get; set; }
        public string NameObject { get; set; }
        public string PhotoObject { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
    }
}
