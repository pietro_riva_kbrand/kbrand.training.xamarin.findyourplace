﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.models
{
    public class Params
    {
        public Params()
        {

        }

        public Params(string name, string surname, string color, string email, string password)
        {
            Name = name;
            Surname = surname;
            Color = color;
            Email = email;
            Password = password;
        }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string Color { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class ItemUpdateParam
    {
        public ItemUpdateParam()
        {
            
        }
        public int Id { get; set; }
        public string Title { get; set; }
    }

    public class DeleteItemParam
    {
        public DeleteItemParam()
        {

        }

        public int Id { get; set; }
    }

    public class ItemParam
    {
        public ItemParam()
        {
        }
        public string Title { get; set; }
        public string Photo { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Address { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
    }

    public class GetListParam
    {
        public GetListParam()
        {
        }
        public int UserId { get; set; }
    }

    public class CreateUserParam
    {
        public CreateUserParam()
        {
        }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Color { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
