﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content;
using Plugin.Permissions;
using Plugin.CurrentActivity;
using Xamarin.Forms;
using App.helpers;
using App.Droid.services;
using Plugin.FirebasePushNotification;

namespace App.Droid
{
    [Activity(Label = "App", Icon = "@drawable/icon", Theme = "@style/MainTheme.Splash", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, WindowSoftInputMode = SoftInput.AdjustResize, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static Context mContext;
        public static bool isStarted { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.SetTheme(Resource.Style.MainTheme_Base);

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            FirebasePushNotificationManager.ProcessIntent(this, Intent);

            mContext = this.ApplicationContext;

            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(this, savedInstanceState);

            MessagingCenter.Subscribe<object>(this, NotificationHelper.GPS_LOCATION_START_REGISTRATION, (sender) =>
            {
                try
                {
                    Context c = MainActivity.getContext();
                    Intent startServiceIntent = new Intent(c, typeof(GpsService));
                    startServiceIntent.SetAction(Android.Provider.Settings.ActionRequestIgnoreBatteryOptimizations);
                    startServiceIntent.SetAction(Constants.ACTION_START_SERVICE);

                    Intent stopServiceIntent = new Intent(c, typeof(GpsService));
                    stopServiceIntent.SetAction(Constants.ACTION_STOP_SERVICE);

                    c.StartService(startServiceIntent);
                    MainActivity.isStarted = true;

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            });

            LoadApplication(new App());
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public static Context getContext()
        {
            return mContext;
        }
    }
}