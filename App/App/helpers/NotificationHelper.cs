﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.helpers
{
    public static class NotificationHelper
    {
        public static string ONMODIFYLIST = "ONMODIFYLIST";

        public static string SEND_GPS_LOCATION_REGISTRATION = "SEND_GPS_LOCATION_REGISTRATION";
        public static string GPS_LOCATION_STOP_REGISTRATION = "SEND_GPS_LOCATION_REGISTRATION";
        public static string GPS_LOCATION_START_REGISTRATION = "GPS_LOCATION_START_REGISTRATION";

        public static int GPS_TRACKING_INTERVAL = 5000;
    }
}
