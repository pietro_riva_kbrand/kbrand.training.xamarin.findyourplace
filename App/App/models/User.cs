﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.models
{
    public class User
    {
        public User()
        {

        }

        public User(int id, string nome, string cognome, string colore, string email)
        {
            Id = id;
            Nome = nome;
            Cognome = cognome;
            Colore = colore;
            Email = email;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Colore { get; set; }
        public string Email { get; set; }
    }
}
