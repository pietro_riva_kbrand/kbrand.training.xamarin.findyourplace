﻿using App.helpers;
using App.interfaces;
using App.models;
using App.services;
using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AddNewItem : ContentPage
	{
        Position pos;
        IGeolocator locator;
        IEnumerable<Address> address;
        Stream newPhoto;
        byte[] file;

        public AddNewItem (Stream photo)
		{
			InitializeComponent ();

            photo.Position = 0;
            file = ImageHelper.StreamToArray(photo);

            photo.Position = 0;
            PhotoImg.Source = ImageSource.FromStream(() => photo);

            NamePlace.TextColor = Color.FromHex(Session.ConnectedUser.Colore);
            AddressLbl.TextColor = Color.FromHex(Session.ConnectedUser.Colore);

            MessagingCenter.Subscribe<object, Position>(this, NotificationHelper.SEND_GPS_LOCATION_REGISTRATION, async (sender, args) =>
            {
                try
                {
                    IndicatorLayout.IsVisible = true;
                    InfoLayout.IsVisible = false;
                    
                    pos = args;

                    if (args != null)
                    {
                        locator = CrossGeolocator.Current;

                        address = await locator.GetAddressesForPositionAsync(args);
                        AddNewBtn.IsEnabled = true;
                        Device.BeginInvokeOnMainThread(() => 
                        {
                            AddressLbl.Text = $"{address.FirstOrDefault().Thoroughfare}, {address.FirstOrDefault().Locality}, {address.FirstOrDefault().CountryName}, {address.FirstOrDefault().CountryCode}";
                            LatitudeLbl.Text = address.FirstOrDefault().Latitude.ToString();
                            LongitudeLbl.Text = address.FirstOrDefault().Longitude.ToString();
                        });
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
                finally
                {
                    InfoLayout.IsVisible = true;
                    IndicatorLayout.IsVisible = false;
                }
            });
        }

        private async void OnAddNewItem(object sender, EventArgs e)
        {
            try
            {
                if(string.IsNullOrWhiteSpace(NamePlace.Text) || string.IsNullOrWhiteSpace(DescriptionEditor.Text))
                {
                    DependencyService.Get<IGetToast>().GetToast("Inserire tutte le informazioni necessarie", string.Empty, false);
                    return;
                }

                ItemParam param = new ItemParam() { Address = $"{address.FirstOrDefault().Thoroughfare}, {address.FirstOrDefault().Locality}, {address.FirstOrDefault().CountryName}, {address.FirstOrDefault().CountryCode}", Description = DescriptionEditor.Text, Latitude = address.FirstOrDefault().Latitude.ToString(), Longitude = address.FirstOrDefault().Longitude.ToString(), Title = NamePlace.Text, UserId = Session.ConnectedUser.Id, Photo = string.Empty };

                var result = await ServiceClient.SendDataContent("InsertItem", param, file);

                if (result != null)
                {
                    bool response = JsonConvert.DeserializeObject<bool>(result);

                    if (response)
                    {
                        MessagingCenter.Send<object>(this, NotificationHelper.GPS_LOCATION_STOP_REGISTRATION);

                        MessagingCenter.Send<object, object>(this, NotificationHelper.ONMODIFYLIST, param);

                        DependencyService.Get<IGetToast>().GetToast("Elemento aggiunto alla lista!", string.Empty, false);

                        await Navigation.PopModalAsync();
                    }
                    else
                    {
                        DependencyService.Get<IGetToast>().GetToast("Errore nel caricamento delle informazioni", string.Empty, false);
                    }
                }
                else
                {
                    DependencyService.Get<IGetToast>().GetToast("Errore nel caricamento delle informazioni", string.Empty, false);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
           
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Send<object>(this, NotificationHelper.GPS_LOCATION_START_REGISTRATION);
        }
    }
}