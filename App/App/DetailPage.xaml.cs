﻿using App.models;
using App.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetailPage : ContentPage
	{
        ListItemObject Item;

		public DetailPage (ListItemObject item)
		{
			InitializeComponent ();
            this.Item = item;

            PhotoImg.Source = Item.PhotoObject;
            AddressLbl.Text = Item.Address;
            AddressLbl.TextColor = Color.FromHex(Session.ConnectedUser.Colore);
            DescriptionLbl.Text = Item.Description;
            CoordinateText.TextColor = Color.FromHex(Session.ConnectedUser.Colore);
            LatitudeLbl.Text = Item.Latitude;
            LongitudeLbl.Text = Item.Longitude;
        }
	}
}