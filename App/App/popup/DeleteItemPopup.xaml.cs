﻿using App.helpers;
using App.interfaces;
using App.models;
using App.services;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App.popup
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DeleteItemPopup : PopupPage
    {
        ListItemObject Item;
        public DeleteItemPopup(ListItemObject item)
        {
            InitializeComponent();
            this.Item = item;

            SubTitleLbl.Text = $"Sei sicuro di voler eliminare {item.NameObject} ?";
            TitleLbl.TextColor = Color.FromHex(Session.ConnectedUser.Colore);
        }

        private async void OnDeleteItem(object sender, EventArgs e)
        {
            DeleteItemParam param = new DeleteItemParam() { Id = Item.Id };

            var result = await ServiceClient.SendData("DeleteItem", param);

            if (result != null)
            {
                bool response = JsonConvert.DeserializeObject<bool>(result);

                if (response)
                {
                    MessagingCenter.Send<object, object>(this, NotificationHelper.ONMODIFYLIST, param);

                    DependencyService.Get<IGetToast>().GetToast("Elemento eliminato!", string.Empty, false);

                    await Navigation.PushModalAsync(new NavigationPage(new MainPage()) { BarBackgroundColor = Color.FromHex(Session.ConnectedUser.Colore), BarTextColor = Color.FromHex("#fff") });

                    await Navigation.PopPopupAsync();
                }
                else
                {
                    DependencyService.Get<IGetToast>().GetToast("Eliminazione non riuscita, riprova più tardi", string.Empty, false);
                }
            }
            else
            {
                DependencyService.Get<IGetToast>().GetToast("Eliminazione non riuscita, riprova più tardi", string.Empty, false);
            }
        }

        private async void OnClosePopup(object sender, EventArgs e)
        {
            await Navigation.PopAllPopupAsync();
        }
    }
}