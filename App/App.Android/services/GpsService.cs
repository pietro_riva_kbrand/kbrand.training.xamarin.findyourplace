﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using App.helpers;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;

namespace App.Droid.services
{
    [Service]
    class GpsService : Service
    {
        IGeolocator locator;

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            try
            {
                if (intent != null && intent.Action.Equals(Constants.ACTION_START_SERVICE))
                {
                    RegisterForegroundService();

                    locator = CrossGeolocator.Current;

                    locatorRestart(NotificationHelper.GPS_TRACKING_INTERVAL);

                    MessagingCenter.Subscribe<object>(this, NotificationHelper.GPS_LOCATION_STOP_REGISTRATION, (sender) =>
                    {
                        locator.StopListeningAsync();
                        StopForeground(true);
                    });
                }
                else if (intent != null && intent.Action.Equals(Constants.ACTION_STOP_SERVICE))
                {
                    StopForeground(true);
                    StopSelf(startId);
                }

                return StartCommandResult.Sticky;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
                return StartCommandResult.Sticky;
            }
        }

        private void Locator_PositionError(object sender, PositionErrorEventArgs e)
        {
            locatorRestart(NotificationHelper.GPS_TRACKING_INTERVAL);
        }

        private void locatorRestart(int minTime)
        {
            if (locator != null)
            {
                locator.StopListeningAsync();

                locator.StartListeningAsync(TimeSpan.FromMilliseconds(minTime), 0, false, new Plugin.Geolocator.Abstractions.ListenerSettings
                {
                    ActivityType = Plugin.Geolocator.Abstractions.ActivityType.Other,
                    AllowBackgroundUpdates = true,
                    DeferLocationUpdates = true,
                    DeferralDistanceMeters = 5,
                    DeferralTime = TimeSpan.FromMilliseconds(60),
                    ListenForSignificantChanges = false,
                    PauseLocationUpdatesAutomatically = false
                });

                locator.PositionChanged += Locator_PositionChanged;
                locator.PositionError += Locator_PositionError;
            }
        }

        private void Locator_PositionChanged(object sender, PositionEventArgs e)
        {
            if (e.Position != null)
            {
                MessagingCenter.Send<object, Position>(this, NotificationHelper.SEND_GPS_LOCATION_REGISTRATION, e.Position);
            }
        }

        void RegisterForegroundService()
        {
            try
            {
                var notification = new NotificationCompat.Builder(this, Constants.CHANNEL_ID)
                    .SetContentTitle("GPS Tracking")
                    .SetContentText($"Registrazione luogo in corso")
                    .SetSmallIcon(Resource.Drawable.big_icon_grey)
                    .SetContentIntent(BuildIntentToShowMainActivity())
                    .SetOngoing(true)
                    .Build();

                if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
                {
                    NotificationChannel mChannel = new NotificationChannel(Constants.CHANNEL_ID, "NotificationChannel", NotificationImportance.High);
                    NotificationManager mNotificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
                    mNotificationManager.CreateNotificationChannel(mChannel);
                }

                StartForeground(Constants.SERVICE_RUNNING_NOTIFICATION_ID, notification);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
        }

        PendingIntent BuildIntentToShowMainActivity()
        {
            var notificationIntent = new Intent(this, typeof(MainActivity));
            notificationIntent.SetAction(Constants.ACTION_MAIN_ACTIVITY);
            notificationIntent.SetFlags(ActivityFlags.SingleTop);
            notificationIntent.PutExtra(Constants.SERVICE_STARTED_KEY, true);

            var pendingIntent = PendingIntent.GetActivity(this, 0, notificationIntent, PendingIntentFlags.UpdateCurrent);
            return pendingIntent;
        }

        public override void OnDestroy()
        {
            try
            {
                var notificationManager = (NotificationManager)GetSystemService(NotificationService);
                notificationManager.Cancel(Constants.SERVICE_RUNNING_NOTIFICATION_ID);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }

            base.OnDestroy();
        }
    }
}