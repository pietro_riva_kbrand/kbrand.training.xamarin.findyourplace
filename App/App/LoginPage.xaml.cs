﻿using App.interfaces;
using App.models;
using App.services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
		}

        private async void OnLoginCliked(object sender, EventArgs e)
        {
            try
            {
                //UI Update
                Indicator.IsVisible = true;
                EmailEntry.IsEnabled = false;
                PasswordEntry.IsEnabled = false;

                if (!string.IsNullOrEmpty(EmailEntry.Text) && !string.IsNullOrWhiteSpace(EmailEntry.Text) && !string.IsNullOrEmpty(PasswordEntry.Text) && !string.IsNullOrWhiteSpace(PasswordEntry.Text))
                {
                    LoginParam param = new LoginParam() { Email = EmailEntry.Text, Password = PasswordEntry.Text };

                    var result = await ServiceClient.SendData("Login", param);

                    if (result != null && !result.Equals("null"))
                    {
                        Session.ConnectedUser = JsonConvert.DeserializeObject<User>(result.ToString());
                        await Navigation.PushModalAsync(new NavigationPage(new MainPage()) { BarBackgroundColor = Color.FromHex(Session.ConnectedUser.Colore), BarTextColor = Color.FromHex("#fff") });
                    }
                    else
                    {
                        DependencyService.Get<IGetToast>().GetToast("Credenziali errate", string.Empty, false);
                    }
                }
                else
                {
                    DependencyService.Get<IGetToast>().GetToast("Devi inserire le credenziali!", string.Empty, false);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            finally
            {
                //UI Update
                Indicator.IsVisible = false;
                EmailEntry.IsEnabled = true;
                PasswordEntry.IsEnabled = true;
            }
        }

        private async void OnRegisterTapped(object sender, TappedEventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new SignUpPage()) { BarBackgroundColor = Color.FromHex("#f9b609"), BarTextColor = Color.FromHex("#fff")});
        }
    }
}