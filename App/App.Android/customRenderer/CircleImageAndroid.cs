﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App.customRenderer;
using App.Droid.customRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CircleImage), typeof(CircleImageAndroid))]
namespace App.Droid.customRenderer
{
    class CircleImageAndroid : ImageRenderer
    {
        private Xamarin.Forms.Color OutlineColor { get; set; }
        private int StrokeWidth { get; set; }
        private bool HasShadow { get; set; }
        private float ShadowWidth { get; set; }
        private Xamarin.Forms.Color ShadowColor { get; set; }
        private float RectRadius { get; set; }
        private ClipType ClipType { get; set; }

        private RectF RoundRect;

        public CircleImageAndroid(Context context) : base(context)
        {
        }

        protected override bool DrawChild(Canvas canvas, global::Android.Views.View child, long drawingTime)
        {
            try
            {
                var radius = Math.Min(Width, Height) / 2;
                radius -= this.StrokeWidth / 2;

                RoundRect = new RectF(this.Left, this.Top, this.Right, this.Bottom);

                //Create path to clip
                var path = new Path();
                if (this.ClipType == ClipType.Circle) path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);
                if (this.ClipType == ClipType.Square) path.AddRoundRect(RoundRect, this.RectRadius, this.RectRadius, Path.Direction.Ccw);
                canvas.Save();
                canvas.ClipPath(path);

                var result = base.DrawChild(canvas, child, drawingTime);

                canvas.Restore();

                // Create path for circle border
                path = new Path();
                if (this.ClipType == ClipType.Circle) path.AddCircle(Width / 2, Height / 2, radius, Path.Direction.Ccw);
                if (this.ClipType == ClipType.Square) path.AddRoundRect(RoundRect, this.RectRadius, this.RectRadius, Path.Direction.Ccw);

                var paint = new Paint();
                paint.AntiAlias = true;
                paint.StrokeWidth = this.StrokeWidth;
                paint.SetStyle(Paint.Style.Stroke);
                if (this.HasShadow) paint.SetShadowLayer((this.ShadowWidth > 0) ? this.ShadowWidth : 1.0f, 0f, 0f, (this.ShadowColor.A != 0 || this.ShadowColor.R != 0 || this.ShadowColor.G != 0 || this.ShadowColor.B != 0) ? this.ShadowColor.ToAndroid() : Android.Graphics.Color.DarkGray);
                paint.Color = (this.OutlineColor.A != 0 || this.OutlineColor.R != 0 || this.OutlineColor.G != 0 || this.OutlineColor.B != 0) ? this.OutlineColor.ToAndroid() : global::Android.Graphics.Color.White;

                canvas.DrawPath(path, paint);

                //Properly dispose
                paint.Dispose();
                path.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Unable to create circle image: " + ex);
            }

            return base.DrawChild(canvas, child, drawingTime);
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
            {
                if ((int)Android.OS.Build.VERSION.SdkInt < 18)
                    //SetLayerType(LayerType.Software, null);
                    return;
            }
            try
            {
                var image = e.NewElement as CircleImage;
                this.OutlineColor = image.OutlineColor;
                this.StrokeWidth = image.StrokeWidth;
                this.HasShadow = image.HasShadow;
                this.ShadowWidth = image.ShadowWidth;
                this.ShadowColor = image.ShadowColor;
                this.RectRadius = image.RectRadius;
                this.ClipType = image.ClipType;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(@"ERROR:", ex.Message);
            }
        }
    }
}