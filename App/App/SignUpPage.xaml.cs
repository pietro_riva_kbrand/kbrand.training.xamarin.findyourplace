﻿using App.interfaces;
using App.models;
using App.services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SignUpPage : ContentPage
	{
		public SignUpPage ()
		{
			InitializeComponent ();

            InitializeView();
		}

        private void InitializeView()
        {
            List<string> Colors = new List<string>();

            Colors.Add("Rosso");
            Colors.Add("Giallo");
            Colors.Add("Verde");
            Colors.Add("Blu");
            Colors.Add("Violetto");
            Colors.Add("Arancione");
            Colors.Add("Indaco");

            ColorPicker.ItemsSource = Colors;
            ColorPicker.SelectedIndex = 0;
        }

        private async void OnSignUpClicked(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(NomeEntry.Text) && !string.IsNullOrWhiteSpace(CognomeEntry.Text) 
                    && !string.IsNullOrWhiteSpace(EmailEntry.Text) && !string.IsNullOrWhiteSpace(PasswordEntry.Text))
                {
                    CreateUserParam param = new CreateUserParam() { Color = ColorPicker.SelectedItem.ToString(), Email = EmailEntry.Text, Name = NomeEntry.Text, Password = PasswordEntry.Text, Surname = CognomeEntry.Text };

                    var result = await ServiceClient.SendData("CreateUser", param);

                    bool response = JsonConvert.DeserializeObject<bool>(result);

                    if (response)
                    {
                        DependencyService.Get<IGetToast>().GetToast("Registrazione Avvenuta!", string.Empty, false);

                        await Navigation.PopModalAsync();
                    }
                    else
                    {
                        DependencyService.Get<IGetToast>().GetToast("Si è verificato un errore.", string.Empty, false);
                    }
                }
                else
                {
                    DependencyService.Get<IGetToast>().GetToast("E' necessario riempire tutti i campi", string.Empty, false);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            } 
        }
    }
}