﻿using Plugin.FirebasePushNotification;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace App
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new NavigationPage(new MainPage()) { BarBackgroundColor = Color.FromHex("#f0c75e") , BarTextColor = Color.FromHex("#fff") };
            MainPage = new LoginPage();

            CrossFirebasePushNotification.Current.OnTokenRefresh += Current_OnTokenRefresh;

            
        }

        private void Current_OnTokenRefresh(object source, Plugin.FirebasePushNotification.Abstractions.FirebasePushNotificationTokenEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Token);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
