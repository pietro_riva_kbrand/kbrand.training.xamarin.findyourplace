﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace App.helpers
{
    public class ImageHelper
    {
        public static byte[] StreamToArray(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
    }
}
