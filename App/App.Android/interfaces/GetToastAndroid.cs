﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App.Droid.interfaces;
using App.interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(GetToastAndroid))]
namespace App.Droid.interfaces
{
    public class GetToastAndroid : IGetToast
    {
        public void GetToast(string message, string title, bool delayLong)
        {
            Toast toast;
            Context c = MainActivity.getContext();

            if (delayLong)
            {
                toast = Toast.MakeText(c, message, ToastLength.Long);
            }
            else
            {
                toast = Toast.MakeText(c, message, ToastLength.Short);
            }
            toast.Show();
        }
    }
}