﻿using App.models;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.services
{
    public static class Session
    {
        public static string Base_URL = "https://kbrand-exercise-test.azurewebsites.net/api/Values/";

        public static User ConnectedUser { get; set; }

        private static List<ListItemObject> placeslist;
        public static List<ListItemObject> PlacesList {
            get
            {
                if (placeslist == null)
                {
                    placeslist = new List<ListItemObject>();
                }
                return placeslist;
            }
            set
            {
                placeslist = value;
            }
        }
    }
}
