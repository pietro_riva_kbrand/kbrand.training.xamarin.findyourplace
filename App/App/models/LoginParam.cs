﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.models
{
    public class LoginParam
    {
        public LoginParam()
        {

        }

        public LoginParam(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public string Email { get; set; }
        public string Password { get; set; }
    }
}
